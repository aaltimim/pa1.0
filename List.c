#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "List.h"


//PrivTypes

//NodeObj
typedef struct NodeObj {
    int data;
    struct NodeObj* next;
    struct NodeObj* prev;
} NodeObj;

//Node 
typedef NodeObj* Node;

//constructor of the Node Type
Node newNode(int data){
    Node N = malloc(sizeof(NodeObj));
    N->data=data;
    N->next= NULL;
    N->prev= NULL;
    return(N);
}
//list of Objects
typedef struct ListObj{
    Node front;
    Node back;
    Node cursor;
    int length;
    int cursorIndex;
}

//destructor for the Node type 
void freeNode(Node* pN){
   if(pN!=NULL && *pN!=NULL){
      free(*pN);
      *pN = NULL;
   }
}


//
//Public Functions
//

//constructor for the List type
List newList(void){
    List L = malloc(sizeof(ListObj));
    L->front=NULL;
    L->back=NULL;
    L->cursor=NULL;
    L->legnth=0;
    L->cursorIndex =-1;
    return(L);
}
//Retyrns the nnumber of elements in List
int length(List L){
    if(L==NULL){
        printf("List Error: calling Length() on NULL List reference\n");
        exit(1);
    }else{
        return L->length;
    }
}

//Destructor for the List or free's the list
void freeList(List* pL){
    if(pL !=NULL && *pL!=NULL){
        clear()
    }
}

//It returns the index of the cursor element
int index(List L){
    if(L==NULL){
        printf("Error: Calling index() on NULL List reference\n");
        exit(1);
    }else{
        if(length(L)>0){
            return L->front->data;
        }else{
            printf("Error: Calling front() on empty List\n");
            exit(1);
        }
    }
}

//Returns the back element of List
int back(List L){
    if(L==NULL){
        printf("Error: Calling back() on NULL List reference\n");
        exit(1);
    }else{
        if(length(L)>0){
            return L->back->data;
        }else{
            printf("Error: Calling back() on empty List\n");
            exit(1);
        }
    }
}

//return cursor element
int get(List L) {
	if(L==NULL) {
		printf("Error: Calling get() on NULL List reference\n");
		exit(1);
	}else {
        if(length(L)>0 && index(L)>=0) {
			return L->cursor->data;
		}else {
			printf("Error: Calling get() on empty List or cursor is undefined\n");
			exit(1);
		}
	}
}

//Return true if this List and L are the same integer sequence
int equals(List A, List B) {
	if(A==NULL || B==NULL) {
		printf("Error: Calling equals() on NULL List reference\n");
		exit(1);
	} else {
		int true=1;
		int false=0;
		Node Aref = NULL;
		Node Bref = NULL;
		moveFront(A);
		moveFront(B);
		if(length(A) == length(B)) {
			Aref = A->cursor;
			Bref = B->cursor;
			for(int i=0; i<length(A);i++) {
				if(Aref->data != Bref->data) {
					return false;
				}
				Aref = Aref->next;
				Bref = Bref->next;
			}return true;
		}return false;
    }
}

//Clears the list, sets list to starting state
void clear(List L) {
	if(L==NULL) {
		printf("Error: Calling clear() on NULL List reference\n");
		exit(1);
	}else {
		while(length(L) > 0) {
			deleteFront(L);
		}
		L->front = NULL;
		L->back = NULL;
		L->cursor = NULL;
		L->length = 0;
		L->cursorIndex = -1;
	}
}

// places cursor under front element of List
void moveFront(List L) {
	if(L==NULL) {
		printf("Error: Calling moveFront() on NULL List reference\n");
		exit(1);
	}else {
        if(length(L)>0) {
		L->cursor = NULL;
		L->cursor = L->front;
		L->cursorIndex = 0;
		}else {
			printf("Error: Calling moveFront() on empty List\n");
			exit(1);
		}
	}
}

// places cursor under the back element of List
void moveBack(List L) {
	if(L==NULL) {
		printf("Error: Calling moveBack() on NULL List reference\n");
		exit(1);
	}else {
	    if(length(L)>0) {
			L->cursor = NULL;
			L->cursor = L->back;
			L->cursorIndex = length(L)-1;
		}else {
			printf("Error: Calling moveBack() on empty List\n");
			exit(1);
		}
	}
}

// moves cursor one step toward front of List unless cursor is undefined or at front of List
void movePrev(List L) {
	if(L==NULL) {
		printf("Error: Calling movePrev() on NULL List reference\n");
		exit(1);
	}else {
		if(index(L) == -1) {}
		else if(index(L) == 0) {
			L->cursor = NULL;
			L->cursorIndex = -1;
		}else {
			L->cursor = L->cursor->prev;
			L->cursorIndex--;
		}
	}
}

// moves cursor one step toward back of List unless cursor is undefined or at back of List
void moveNext(List L) {
	if(L==NULL) {
		printf("Error: Calling moveNext() on NULL List reference\n");
		exit(1);
	}else {
        if(index(L) == -1) {}
		else if(index(L) == length(L)-1) {
			L->cursor = NULL;
			L->cursorIndex = -1;
		}else {
			L->cursor = L->cursor->next;
			L->cursorIndex++;
		}
	}
}

// inserts new element at front of List if List is empty, becomes first element in List 
void prepend(List L, int data) {
	if(L==NULL) {
		printf("Error: Calling prepend() on NULL List reference\n");
		exit(1);
	}else {
		Node N = newNode(data);
		if(length(L) == 0) {
			L->front = N;
			L->back = N;
		}else {
			Node temp = L->front;
			N->next = temp;
			temp->prev = N;
			L->front = N;
			N->prev = NULL;
			if(index(L) >= 0) {
				L->cursorIndex++;
			}
		}
		L->length++;
	}
}

// inserts new element at back of List if List is empty, becomes first element in List 
void append(List L, int data) {
	if(L==NULL) {
		printf("Error: Calling append() on NULL List reference\n");
		exit(1);
	}else {
		Node N = newNode(data);
		if(length(L)==0) {
			L->front = N;
			L->back = N;
		}else {
			Node temp = L->back;
			N->prev = temp;
			temp->next = N;
			L->back = N;
			N->next = NULL;
		}
		L->length++;
	}
}

// inserts new element before cursor
void insertBefore(List L, int data) {
	if(L==NULL) {
		printf("Error: Calling insertBefore() on NULL List reference\n");
		exit(1);
	}else {
		if(length(L)>0 && index(L)>=0) {
			Node N = newNode(data);
			if(L->cursor->prev != NULL) {
				Node temp = L->cursor->prev;
				temp->next = N;
				N->prev = temp;
			}
			N->next = L->cursor; 
			L->cursor->prev = N;
			L->cursorIndex++;
			L->length++;
		}else {
			printf("Error: Calling insertBefore() on empty List or cursor is undefined\n");
			exit(1);
		}
	}
}

// inserts new element after cursor
void insertAfter(List L, int data) {
	if(L==NULL) {
		printf("Error: Calling insertAfter() on NULL List reference\n");
		exit(1);
	}else {
		if(length(L)>0 && index(L)>=0 ) {
			Node N = newNode(data);
			if(L->cursor->next != NULL) {
				Node temp = L->cursor->next;
				N->next = temp;
				L->cursor->next = N;
				temp->prev = N;
				N->prev = L->cursor;
			}else {
				L->cursor->next = N;
				N->prev = L->cursor;
				N->next = NULL;
			}
			L->length++;
		}else {
			printf("Error: Calling insertAfter() on empty List or cursor is undefined\n");
			exit(1);
		}
	}
}

// deletes the front element 
void deleteFront(List L) {
	if(L==NULL) {
		printf("Error: Calling deleteFront() on NULL List reference\n");
		exit(1);
	}else {
		if(length(L)>0) {
			Node temp = L->front;
			L->front = L->front->next;
			freeNode(&temp);
			if(index(L)== 0) {
				L->cursor=NULL;
				L->cursorIndex=-1;
			}
			L->length--;
		}else {
			printf("Error: Calling deleteFront() on empty List\n");
			exit(1);
		}
	}
}

// deletes the back element
void deleteBack(List L) {
	if(L==NULL) {
		printf("Error: Calling deleteBack() on NULL List reference\n");
		exit(1);
	}else {
		if(length(L)>0) {
			Node temp = L->back;
			L->back = L->back->prev;
			freeNode(&temp);
			L->length--;
			if(index(L)== length(L)) {
				L->cursor=NULL;
				L->cursorIndex = -1;
			}
		}else {
			printf("Error: Calling deleteBack() on empty List\n");
			exit(1);
		}
	}
}

// deletes cursor element, making cursor undefined
void delete(List L) {
	if(L==NULL) {
		printf("Error: Calling delete() on NULL List reference\n");
		exit(1);
	}else {
		if(length(L)>0 && index(L)>=0) {
			if(length(L)==1) {
				clear(L);
			}else if(L->cursor->prev != NULL) {
				Node deletedNode = L->cursor;
				Node temp = L->cursor->prev;
				temp->next =L->cursor->next;
				freeNode(&deletedNode);
				L->cursor = NULL;
				L->cursorIndex = -1;
				L->length--;
			}else {
				deleteFront(L);
			}
		}else {
			printf("Error: Calling delete() on empty List or cursor is undefined\n");
			exit(1);
		}
	}
}

// prints List to out file
void printList(FILE* out, List L) {
	if(L==NULL) {
		printf("Error: calling printList() on NULL List reference\n");
		exit(1);
	}else {
		Node N = L->front;
		while(N != NULL) {
			fprintf(out, "%d ", N->data);
			N = N->next;
		}
	}
}

// returns a new List representing the same integer sequence as this List
List copyList(List L) {
	if(L==NULL) {
		printf("Error: Calling copyList() on NULL List reference\n");
		exit(1);
	}else {
		List returnList = newList();
		Node N = L->front;
		for(int i=0; i<length(L); i++) {
			append(returnList,N->data);
			N=N->next;
		}
		return returnList;
	}
}

// returns a new List which is the concatenation of this list followed by L
List concatList(List A, List B) {
	if(A==NULL || B==NULL) {
		printf("Error: Calling concatList() on NULL List reference\n");
		exit(1);
	}else {
		List returnList = newList();
		Node Anode = A->front;
		for(int i=0; i<length(A); i++) {
			append(returnList, Anode->data);
			Anode = Anode->next;
		}
		Node Bnode = B->front;
		for(int i=0; i<length(B); i++) {
			append(returnList, Bnode->data);
			Bnode = Bnode->next;
		}
		return returnList;
	}
}
